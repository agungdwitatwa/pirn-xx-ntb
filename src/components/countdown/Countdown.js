import { useState, useEffect } from "react";
import { getRemainingTimeUntilMsTimestamp } from "./Utils/CountdownTimerUtils";
import "./style/countdown.scss";

const defaultRemainingTime = {
  seconds: "00",
  minutes: "00",
  hours: "00",
  days: "00",
};

const Countdown = ({ countdownTimestampMs }) => {
  const [remainingTime, setRemainingTime] = useState(defaultRemainingTime);

  useEffect(() => {
    const intervalId = setInterval(() => {
      updateRemainingTime(countdownTimestampMs);
    }, 1000);
    return () => clearInterval(intervalId);
  }, [countdownTimestampMs]);

  function updateRemainingTime(countdown) {
    setRemainingTime(getRemainingTimeUntilMsTimestamp(countdown));
  }

  return (
    <div className="count">
      <div className="days">{remainingTime.days} Hari</div>
      <div className="hour">{remainingTime.hours} Jam</div>
      <div className="minutes">{remainingTime.minutes} Menit</div>
      <div className="seconds">{remainingTime.seconds} Detik</div>
    </div>
  );
};

export default Countdown;
