import "./style/lokasi-kegiatan.scss";
import { useState } from "react";

export default function LokasiKegiatan() {
  const [isItemOneClicked, setIsItemOneClicked] = useState(false);
  const [isItemTwoClicked, setIsItemTwoClicked] = useState(false);
  const [isItemThreeClicked, setIsItemThreeClicked] = useState(false);

  return (
    <div className="lokasi-kegiatan" id="lokasi-kegiatan">
      <div className="title">
        <h2>Lokasi Kegiatan</h2>
      </div>
      <div className="lokasi">
        <div
          className={`item ${isItemOneClicked ? "clicked" : ""}`}
          onClick={() => {
            setIsItemOneClicked(!isItemOneClicked);
            setIsItemTwoClicked(false);
            setIsItemThreeClicked(false);
          }}
        >
          <h4 className={`${isItemOneClicked ? "close-initial" : ""}`}>
            Kantor <br /> Gubernur NTB
          </h4>
          <div className={`lokasi-desc ${isItemOneClicked ? "open-desc" : ""}`}>
            <h4>
              Kantor <br /> Gubernur NTB
            </h4>
            <p>
              Pemerintah Provinsi Nusa Tenggara Barat sebagai tuan rumah Pekan
              PIRN XX akan menyambut peserta sekaligus membuka Pekan PIRN XX NTB
              di Kantor Gubernur NTB. Terletak di Pusat kota, Kantor BPSDM NTB
              akan menjadi lokasi kegiatan kelas selama Pekan PIRN XX NTB
              sedangkan, penutupan dan malam keakraban Pekan PIRN XX NTB akan
              dilaksanakan di BRIDA NTB sebagai mitra penyelenggara.
            </p>
          </div>
          <div className="overlay" />
        </div>
        <div
          className={`item ${isItemTwoClicked ? "clicked" : ""}`}
          onClick={() => {
            setIsItemTwoClicked(!isItemTwoClicked);
            setIsItemOneClicked(false);
            setIsItemThreeClicked(false);
          }}
        >
          <h4 className={`${isItemTwoClicked ? "close-initial" : ""}`}>
            Gili Trawangan <br /> Gili Meno <br /> Gili Air
          </h4>
          <div className={`lokasi-desc ${isItemTwoClicked ? "open-desc" : ""}`}>
            <h4>Gili Trawangan, Gili Meno, Gili Air</h4>
            <p>
              Taman Wisata Perairan (TWP) Gili Trawangan, Gili Meno, dan Gili
              Air atau yang biasa disebut Gili Tramena merupakan kawasan yang
              kaya akan keanekaragaman hayati laut. Gili Tramena dengan
              keindahan pantai pasir putih dan gradasi warna laut yang biru
              kehijauan serta keramahan penduduknya akan menjadi lokasi
              pengambilan data oleh Peserta Pekan PIRN XX NTB. Lokasi ini
              dipilih juga sebagai bentuk pemulihan Ekonomi Gili Tramena karena
              terdampak Pandemi Covid-19.
            </p>
          </div>
          <div className="overlay" />
        </div>
        <div
          className={`item ${isItemThreeClicked ? "clicked" : ""}`}
          onClick={() => {
            setIsItemThreeClicked(!isItemThreeClicked);
            setIsItemOneClicked(false);
            setIsItemTwoClicked(false);
          }}
        >
          <h4 className={`${isItemThreeClicked ? "close-initial" : ""}`}>
            Laboratorium <br /> Universitas <br /> Mataram
          </h4>
          <div
            className={`lokasi-desc ${isItemThreeClicked ? "open-desc" : ""}`}
          >
            <h4>Laboratorium Universitas Mataram</h4>
            <p>
              Laboratorium sebagai tempat untuk melakukan ekseperimen atau riset
              ilmiah tentu diperlukan oleh Peserta Pekan PIRN XX NTB untuk
              melengkapi pembelajaran risetnya. Oleh karena itu, Peserta Pekan
              PIRN XX NTB juga akan difasilitasi dengan Laboratorium di
              Universitas Mataram untuk dapat menguji sampel yang telah di ambil
              di Gili Trawangan.
            </p>
          </div>
          <div className="overlay" />
        </div>
      </div>
    </div>
  );
}
