import "./style/berita-kegiatan.scss";
import { IoCalendarSharp } from "react-icons/io5";
import { useEffect, useState } from "react";
import { createClient } from "contentful";

const client = createClient({
  space: process.env.REACT_APP_CONTENTFUL_SPACE,
  accessToken: process.env.REACT_APP_CONTENTFUL_TOKEN,
});

export default function BeritaKegiatan() {
  const [beritaItems, setBeritaItems] = useState("");
  const getBerita = async () => {
    const berita = await client.getEntries({
      content_type: "berita",
      select: "fields",
      order: "-fields.tanggalBerita",
      "metadata.tags.sys.id[all]": "pirnxxntb",
    });
    return berita;
  };
  useEffect(() => {
    getBerita().then((res) => {
      setBeritaItems(res);
    });
  }, []);

  return (
    <div className="berita" id="berita-kegiatan">
      <h2>Kegiatan Pekan PIRN XX NTB 2022</h2>
      <div className="berita-content">
        {beritaItems?.items?.map((item, index) => (
          <a
            href={`https://brida.ntbprov.go.id/berita/${item.fields.slug}`}
            className="berita-item"
            key={index}
            id={item.fields.slug}
          >
            <div className="berita-gambar">
              <img
                src={`https:${item.fields.thumbnail?.fields.file.url}`}
                alt="Gambar berita"
              />
            </div>
            <div className="berita-desc">
              <div className="berita-date">
                <IoCalendarSharp className="date-icon" size={22} />
                {item.fields.tanggalBerita?.split("-").reverse().join("-")}
              </div>
              <h4>
                {item.fields.judulBerita?.substring(0, 40)}
                {item.fields.judulBerita?.length > 40 ? ". . ." : ""}
              </h4>
              <p>
                {item.fields.preview?.substring(0, 180)}
                {item.fields.preview?.length > 180 ? ". . ." : ""}
              </p>
            </div>
          </a>
        ))}
      </div>
    </div>
  );
}
