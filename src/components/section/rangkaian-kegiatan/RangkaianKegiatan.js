import "./style/rangkaian-kegiatan.scss";

function RangkaianKegiatan() {
  return (
    <div className="rangkaian-kegiatan" id="rangkaian-kegiatan">
      <h4>Rangkaian Kegiatan PIRN XX NTB 2022</h4>
      <div className="rangkaian-acara">
        <div>
          <h5>Pembukaan PIRN XX NTB 2022</h5>
          <p>10-11 Juli</p>
        </div>
        <div>
          <h5>Penelitian Lapangan</h5>
          <p>12-14 Juli 2022</p>
        </div>
        <div>
          <h5>Presentasi Penelitian dan Penutupan</h5>
          <p>15 Juli 2022</p>
        </div>
        <div>
          <h5>Field Trip</h5>
          <p>16 Juli 2022</p>
        </div>
      </div>
    </div>
  );
}

export default RangkaianKegiatan;
