import "./style/header.scss";
import { useState } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import { ImLocation2 } from "react-icons/im";
import { MdEventNote } from "react-icons/md";
import { BsNewspaper } from "react-icons/bs";
import { AiFillCloseCircle } from "react-icons/ai";

export default function Header() {
  const [showDrawer, setShowDrawer] = useState(false);
  return (
    <div className="header">
      <a href="https://brida.ntbprov.go.id" className="logo">
        <img src="/logo.png" alt="logo BRIDA NTB" />
        <div className="text">
          <h1>BRIDA NTB</h1>
          <p>Badan Riset dan Inovasi Daerah</p>
        </div>
      </a>
      <div className="menu">
        <ul>
          <li>
            <a href="#lokasi-kegiatan">LOKASI KEGIATAN</a>
          </li>
          <li>
            <a href="#rangkaian-kegiatan">RANGKAIAN KEGIATAN</a>
          </li>
          <li>
            <a href="#berita-kegiatan">BERITA DAN KEGIATAN</a>
          </li>
        </ul>
      </div>
      <div className="drawer-menu" onClick={() => setShowDrawer(true)}>
        <div className="button-drawer">
          <GiHamburgerMenu />
        </div>
      </div>
      <div className={`drawer-menu-list ${showDrawer ? "open" : ""}`}>
        <div className="close-drawer-menu" onClick={() => setShowDrawer(false)}>
          <AiFillCloseCircle />
        </div>
        <ul>
          <li>
            <a href="#lokasi-kegiatan">
              <div>
                <ImLocation2 />
                Lokasi Kegiatan
              </div>
            </a>
          </li>
          <li>
            <a href="#rangkaian-kegiatan">
              <div>
                <MdEventNote />
                Rangkaian Kegiatan
              </div>
            </a>
          </li>
          <li>
            <a href="#berita-kegiatan">
              <div>
                <BsNewspaper />
                Berita dan Kegiatan
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
