import "./style/footer.scss";

function Footer() {
  return (
    <div className="footer">
      <h2>PROVIDED BY</h2>
      <div className="provider">
        <div className="provider-item">
          <div className="provider-image">
            <img src="/logo.png" alt="logo brin" />
          </div>
          <div className="provider-name">
            <h3>BRIN</h3>
            <p>Badan Riset dan Inovasi Nasional</p>
          </div>
        </div>
        <div className="provider-item">
          <div className="provider-image">
            <img src="/logo-pemprov.png" alt="logo brin" />
          </div>
          <div className="provider-name">
            <h3>Pemerintah Provinsi </h3>
            <p>Nusa Tenggara Barat</p>
          </div>
        </div>
        <div className="provider-item">
          <div className="provider-image">
            <img src="/logo.png" alt="logo brin" />
          </div>
          <div className="provider-name">
            <h3>BRIDA NTB</h3>
            <p>Badan Riset dan Inovasi Daerah</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
