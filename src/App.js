import "style/App.scss";
import Header from "components/header/Header";
import Hero from "components/section/hero/Hero";
import LokasiKegiatan from "components/section/lokasi-kegiatan/LokasiKegiatan";
import RangkaianKegiatan from "components/section/rangkaian-kegiatan/RangkaianKegiatan";
import BeritaKegiatan from "components/section/berita-kegiatan/BeritaKegiatan";
import Footer from "components/footer/Footer";

function App() {
  return (
    <>
      <div className="head">
        <div className="wrapper">
          <Header />
          <Hero />
        </div>
      </div>
      <div className="section">
        <LokasiKegiatan />
        <RangkaianKegiatan />
        <BeritaKegiatan />
      </div>
      <Footer />
    </>
  );
}

export default App;
